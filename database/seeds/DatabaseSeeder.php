<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // === USERS init seeder
        if(!DB::table('users')->where('email', 'suxin_admin@admin.com')->first()) {
            DB::table('users')->insert([
                'name' => 'admin',
                'email' => 'suxin_admin@admin.com',
                'password' => bcrypt('suxin_admin_655'),
                'role' => 1
            ]);
        }

        Model::reguard();
    }
}
